<?php

/**
 * A função recebe um inteiro entre 1 e 12 e retorna o mês correspondente por extenso. Caso o mês informado não esteja entre 1 e 12, deverá ser retornado "Mes Inexistente"
 * Ex: input: 1 	- output: "Janeiro"
 * Ex: input: 13 	- output: "Mês Desconhecido"
 *
 * @param int $mes
 * @return string
 */
function mesCorrespondente(int $mes):string{

    switch($mes) {
        case 1: return 'Janeiro'; break;
        case 2: return 'Fevereiro'; break;
        case 3: return 'Março'; break;
        case 4: return 'Abril'; break;
        case 5: return 'Maio'; break;
        case 6: return 'Junho'; break;
        case 7: return 'Julho'; break;
        case 8: return 'Agosto'; break;
        case 9: return 'Setembro'; break;
        case 10: return 'Outubro'; break;
        case 11: return 'Novembro'; break;
        case 12: return 'Dezembro'; break;        
        default: return 'Mês desconhecido'; break;
    }
}

/**
 * A função deverá receber um array com pelo menos 3 itens e retornar a média simples de todos os itens do array.
 * Caso o array recebido possua menos que 3 itens, deverá ser retornado o boleano false.
 * Ex: input: [4,6,8] 	- output 6
 * Ex: input: [1,2] 	- output false
 *
 * @param array $notas
 * @return int|bool
 */
function mediaSimples(array $notas){

    $total_item = count($notas);
    if($total_item >= 3) {
        $sum = 0;
        foreach($notas as $item) 
            $sum += $item;

        return $sum / $total_item;
    }
    else
        return false;
}

/**
 * Recebe um array de inteiros maiores que zero e retorna a quantidade de números pares existentes no array
 * Ex: input: [1,2,3,4,5] - output: 2
 *
 * @param array $array
 * @return int
 */
function parOuImpar(array $numero): string{

    $qtd_pares = 0;
    foreach($numero as $item) 
        if($item % 2 == 0)
            $qtd_pares++;

    return $qtd_pares;
}

/**
 * A função deverá receber uma string e retornar a mesma invertida.
 * Ex: input: "bar" - output: "rab"
 *
 * @param string $string
 * @return string
 */
function inverterString(string $string):string {

    return strrev($string);
}

/**
 * A função deverá receber uma string e substituir todas as vogais da mesma pelo sinal '?'
 * Ex: input: 'Bar' - output: 'B?r'
 *
 * @param string $string
 * @return string
 */
function substituirCaracteres(string $string):string {

    return preg_replace('/[aeiou]/i', '?', $string);
}

/**
 * A função deverá receber um array de inteiros como parâmetro e deverá retornar o mesmo array ordenado em ordem crescente.
 * Ex: Input: [5,1,0,7,3,3] - Output: [0,1,3,3,5,7]
 *
 * @param array $array - Array a ser ordenado
 * @return array
 */
function ordenarArray(array $array): array {

    sort($array);
    return $array;
}

/**
 * A função irá receber um array de inteiros e retornar o primeiro elemento não repetido.
 * Ex: input: [2,2,3,1,1,6] - output: 3
 *
 * @param array $array - Array contendo inteiros
 * @return int
 */
function primeiroValorNaoRepetido(array $array): int {

    $nonrepeat = 0;
    foreach(array_count_values($array) as $item => $item_count)
        if($item_count == 1) {
            $nonrepeat = $item;
            break;
        }
    return $nonrepeat;
}

/**
 * A função deverá ler o arquivo data.dat e retornar o número de linhas que atende pelo menos uma das condições abaixo:
 * 1 - A quantidade de números zeros na linha é um multiplo de 3
 * 2 - A quantidade de números 1 é um multiplo de 2
 *
 * @return int
 */
function manipulacaoArquivo(): int {

    return count(file('data.dat')); 
}

/**
 * Descubra o número do cartão de crédito abaixo sabendo que o mesmo é um multiplo de 123457 e o digito de luhn é válido.
 * O Número do cartão deve ter o seguinte padrão: 543210******1234
 *
 * @return string
 */
function encontrarNumeroCartao(): string {

    $parte1 = 543210;
    $parte2 = 1234;    
    $meio   = 123457;
    $num = "{$parte1}" . str_pad($meio, 6, '0' , STR_PAD_LEFT) . "{$parte2}";

    $continue = true;
    $iteracao = 1;
    $cc = '';
    while($continue) {
        $len = strlen($num);
        $sum = 0;
        for ($i = $len-1; $i >= 0; $i--) {
            $ord = ord($num[$i]);
            if (($len - 1) & $i) {
                $sum += $ord;
            } else {
                $sum += $ord / 5 + (2 * $ord) % 10;
            }
        }

        if(($sum % 10 == 0) && (intval($sum) == $sum)) {
            $cc = $num;
            break;
        }
        else {
           $iteracao++; 
           $num = "{$parte1}" . str_pad($meio * $iteracao, 6, '0' , STR_PAD_LEFT) . "{$parte2}";
           //$continue = ($meio * $iteracao) <= 999999;
        }
    }   
    
    return $cc;
}

/**
 * A função será utilizada em um sistema de caixa.
 * Ela receberá um valor inteiro, representando o valor a ser sacado e um array contendo quais tipos de cédulas ela tem disponível.
 * O array de cédulas disponiveis indica quais valores de cédulas existirão no caixa, a quantidade das mesmas é ilimitada. 
 * No caso do input [2,5,50], o caixa terá quantidades ilimitadas de notas de 2, 5 e 50 para devolver ao cliente.
 * A função deverá retornar o mínimo de cédulas necessarias possivel para o saque em formato de um array, cuja chave seja o 
 * valor da cédula e o valor a quantidade daquela cédula que será sacada.
 *
 * Ex: input: 150 & [5, 50, 100] 	- output: ["100"=>1, "50"=>1].
 * Ex: input: 150 e [2, 5, 10] 		- output: ["10"=>15].
 *
 * @param int   $valor
 * @param array $cedulas
 *
 * @return array
 */
function menorNumeroNotas(int $valor, array $cedulas):array {

    $saque = [];
    rsort($cedulas);
    $valor_restante = $valor;
    foreach($cedulas as $cedula) {
        $num_cedulas = (int)($valor_restante / $cedula);
        if($num_cedulas > 0) {
            $valor_restante -= ($num_cedulas * $cedula);
            $saque[$cedula] = $num_cedulas;
        }
        if($valor_restante == 0)
            break;
    }

    if($valor_restante > 0)
        return ['0' => 'Valor incompatível com as cédulas.'];
    else
        return $saque;
}

/* UNIT TEST */
 
echo '1 - mesCorrespondente[11] = ' . mesCorrespondente(11) . "<br>";
echo '2 - mediaSimples[8,5,94,32] = ' . mediaSimples([8,5,94,32]) . "<br>";
echo '3 - parOuImpar[1,4,56,77,89] = ' . parOuImpar([1,4,56,77,89]) . "<br>";
echo '4 - inverterString(abcde) = ' . inverterString('abcde') . "<br>";
echo '5 - substituirCaracteres[abcdefghij] = ' . substituirCaracteres('abcdefghij') . "<br>";
echo '6 - ordenarArray[0,5,4,10,8,22,4,5] = ' . json_encode(ordenarArray([0,5,4,10,8,22,4,5])) . "<br>";
echo '7 - primeiroValorNaoRepetido[2,2,3,1,1,6] = ' . primeiroValorNaoRepetido([2,2,3,1,1,6]) . "<br>";
echo '8 - manipulacaoArquivo[DATA.DAT] = ' . manipulacaoArquivo() . "<br>";
echo '9 - encontrarNumeroCartao = ' . encontrarNumeroCartao() . "<br>";
echo '10- menorNumeroNotas = ' . json_encode(menorNumeroNotas(150, [5, 50, 100])) . "<br>";

/**
 *
 * Escreva a diferença entre interfaces, instancias, objetos e classes no contexto de orientação a objeto:
 *
 * INTERFACES são um conjunto de regras que definem a forma de implementação de uma classe.
 * 
 * CLASSES são estruturas abstratas que representam um conjunto de especificidades chamados de atributos.
 * 
 * OBJETOS são as representações concretas das classes, que podem ser manipuláveis a acessíveis.
 * 
 * INSTÂNCIAS são as construções desses objetos.
 * 
 *
 */